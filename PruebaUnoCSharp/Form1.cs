﻿using PruebaUnoCSharp.ClaseModelo;
using PruebaUnoCSharp.enties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PruebaUnoCSharp
{
    public partial class FrmPrincipal : Form
    {
        private DataTable dtUsuarios;
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
            dtUsuarios = dsUsuario.Tables["Usuario"];
            foreach (Usuario p in UsuarioModel.getListaUsuarios())
            {
                dtUsuarios.Rows.Add(p.Id, p.Nombre_usuario, p.Contraseña, p.Confirmar_contraseña,
                    p.Correo, p.Confirmar_correo, p.Telefono, p.Confirmar_telefono, p.Cantidad_accesos);
            }
        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frmgestion fgp = new Frmgestion();
            // fgp.MdiParent = Value;
            fgp.DsUsuarios = dsUsuario;
            fgp.Show();

        }
    }
}
