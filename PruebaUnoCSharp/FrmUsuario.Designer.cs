﻿namespace MiPrimerProyecto
{
    partial class FrmUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtnombreusuario = new System.Windows.Forms.MaskedTextBox();
            this.txtcontraseña = new System.Windows.Forms.MaskedTextBox();
            this.txtconfcontraseña = new System.Windows.Forms.MaskedTextBox();
            this.txtcorreo = new System.Windows.Forms.MaskedTextBox();
            this.txtconfcorreo = new System.Windows.Forms.MaskedTextBox();
            this.txttelefono = new System.Windows.Forms.MaskedTextBox();
            this.txtconftelefono = new System.Windows.Forms.MaskedTextBox();
            this.txtaccesos = new System.Windows.Forms.MaskedTextBox();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(32, 59);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre de usuario:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(32, 101);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = ":Contraseña";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(32, 138);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(100, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = ":cConfirmar contraseña";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(32, 177);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(100, 23);
            this.label4.TabIndex = 3;
            this.label4.Text = ":Correo";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(32, 210);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(100, 23);
            this.label5.TabIndex = 4;
            this.label5.Text = ":Confirmar correo";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(32, 245);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(100, 23);
            this.label6.TabIndex = 5;
            this.label6.Text = ":Telefono";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(32, 277);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(100, 23);
            this.label7.TabIndex = 6;
            this.label7.Text = ":Confirmar telef.";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(32, 312);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(121, 23);
            this.label8.TabIndex = 7;
            this.label8.Text = ":Cantidad de accesos fallidos";
            // 
            // txtnombreusuario
            // 
            this.txtnombreusuario.Location = new System.Drawing.Point(159, 51);
            this.txtnombreusuario.Name = "txtnombreusuario";
            this.txtnombreusuario.Size = new System.Drawing.Size(214, 20);
            this.txtnombreusuario.TabIndex = 8;
            // 
            // txtcontraseña
            // 
            this.txtcontraseña.Location = new System.Drawing.Point(159, 93);
            this.txtcontraseña.Name = "txtcontraseña";
            this.txtcontraseña.Size = new System.Drawing.Size(214, 20);
            this.txtcontraseña.TabIndex = 9;
            // 
            // txtconfcontraseña
            // 
            this.txtconfcontraseña.Location = new System.Drawing.Point(159, 130);
            this.txtconfcontraseña.Name = "txtconfcontraseña";
            this.txtconfcontraseña.Size = new System.Drawing.Size(214, 20);
            this.txtconfcontraseña.TabIndex = 10;
            // 
            // txtcorreo
            // 
            this.txtcorreo.Location = new System.Drawing.Point(159, 169);
            this.txtcorreo.Name = "txtcorreo";
            this.txtcorreo.Size = new System.Drawing.Size(214, 20);
            this.txtcorreo.TabIndex = 11;
            // 
            // txtconfcorreo
            // 
            this.txtconfcorreo.Location = new System.Drawing.Point(159, 202);
            this.txtconfcorreo.Name = "txtconfcorreo";
            this.txtconfcorreo.Size = new System.Drawing.Size(214, 20);
            this.txtconfcorreo.TabIndex = 12;
            // 
            // txttelefono
            // 
            this.txttelefono.Location = new System.Drawing.Point(159, 237);
            this.txttelefono.Name = "txttelefono";
            this.txttelefono.Size = new System.Drawing.Size(214, 20);
            this.txttelefono.TabIndex = 13;
            // 
            // txtconftelefono
            // 
            this.txtconftelefono.Location = new System.Drawing.Point(159, 269);
            this.txtconftelefono.Name = "txtconftelefono";
            this.txtconftelefono.Size = new System.Drawing.Size(214, 20);
            this.txtconftelefono.TabIndex = 14;
            // 
            // txtaccesos
            // 
            this.txtaccesos.Location = new System.Drawing.Point(159, 312);
            this.txtaccesos.Name = "txtaccesos";
            this.txtaccesos.Size = new System.Drawing.Size(214, 20);
            this.txtaccesos.TabIndex = 15;
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(202, 358);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(75, 23);
            this.btnAgregar.TabIndex = 16;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // FrmUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 399);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.txtaccesos);
            this.Controls.Add(this.txtconftelefono);
            this.Controls.Add(this.txttelefono);
            this.Controls.Add(this.txtconfcorreo);
            this.Controls.Add(this.txtcorreo);
            this.Controls.Add(this.txtconfcontraseña);
            this.Controls.Add(this.txtcontraseña);
            this.Controls.Add(this.txtnombreusuario);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmUsuario";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "FrmUsuario";
            this.Load += new System.EventHandler(this.FrmUsuario_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox txtnombreusuario;
        private System.Windows.Forms.MaskedTextBox txtcontraseña;
        private System.Windows.Forms.MaskedTextBox txtconfcontraseña;
        private System.Windows.Forms.MaskedTextBox txtcorreo;
        private System.Windows.Forms.MaskedTextBox txtconfcorreo;
        private System.Windows.Forms.MaskedTextBox txttelefono;
        private System.Windows.Forms.MaskedTextBox txtconftelefono;
        private System.Windows.Forms.MaskedTextBox txtaccesos;
        private System.Windows.Forms.Button btnAgregar;
    }
}