﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiPrimerProyecto
{
    public partial class FrmUsuario : Form
    {
        private DataTable tblusuarios;
        private DataSet dsUsuario;
        private BindingSource bsUsuario;
        private DataRow drUsuario;
        private int con = 0;
        public DataTable Tblusuarios
        {

            set
            {
                tblusuarios = value;
            }


        }

        public DataSet DsUsuario
        {
           

            set
            {
                dsUsuario = value;
            }
        }

        public FrmUsuario()
        {
            InitializeComponent();
            bsUsuario = new BindingSource();
        }


        public DataRow DrUsuario
        {
            set
            {
                drUsuario = value;
                txtnombreusuario.Text = drUsuario["Usuario"].ToString();
                txtcontraseña.Text = drUsuario["Contraseña"].ToString();
                txtconfcontraseña.Text = drUsuario["ContraseñaC"].ToString();
                txtcorreo.Text = drUsuario["Correo"].ToString();
                txtconfcorreo.Text = drUsuario["CorreoC"].ToString();
                txttelefono.Text = drUsuario["Telefono"].ToString();
                txtconftelefono.Text = drUsuario["TelefonoC"].ToString();
                txtaccesos.Text = drUsuario["Accesos"].ToString();

            }


        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            string nombreU, 
                contraseña,
                confircontra,
                correo, conficorreo,
                telefono,
                confirtelefono,
                accesos; ;

            nombreU = txtnombreusuario.Text;
            contraseña = txtcontraseña.Text;
            confircontra = txtconfcontraseña.Text;
            correo = txtcorreo.Text;
            conficorreo = txtconfcorreo.Text;
            telefono = txttelefono.Text;
            confirtelefono = txtconftelefono.Text;

            if (contraseña != confircontra || correo != conficorreo)
            {
                MessageBox.Show("NO PUEDEN SER DISTINTAS LAS CREDENCIALES!!!");
            }
            else
            {


                //  accesos = txtaccesos.Text;
                if (drUsuario != null)
                {
                    DataRow drNew = tblusuarios.NewRow();

                    int index = tblusuarios.Rows.IndexOf(drUsuario);
                    drNew["Id"] = drUsuario["Id"];
                    drNew["Usuario"] = nombreU;
                    drNew["Contraseña"] = contraseña;
                    drNew["ContraseñaC"] = confircontra;
                    drNew["Correo"] = correo;
                    drNew["CorreoC"] = conficorreo;
                    drNew["Telefono"] = telefono;
                    drNew["TelefonoC"] = confirtelefono;
                    //  drNew["Accesos"] = accesos;


                    tblusuarios.Rows.RemoveAt(index);
                    tblusuarios.Rows.InsertAt(drNew, index);

                }
                else
                {
                    tblusuarios.Rows.Add(tblusuarios.Rows.Count + 1, nombreU,
                        contraseña, confircontra, correo, conficorreo, telefono,
                       confirtelefono, ++con);
                }
            }
        }

        private void FrmUsuario_Load(object sender, EventArgs e)
        {
            bsUsuario.DataSource = dsUsuario;
            bsUsuario.DataMember = dsUsuario.Tables["Usuario"].ToString();
        }
    }
}
