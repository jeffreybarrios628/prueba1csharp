﻿using MiPrimerProyecto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PruebaUnoCSharp
{
    public partial class Frmgestion : Form
    {
        private DataSet dsUsuarios;
        private BindingSource bsUsuarios;

        public DataSet DsUsuarios
        {
            

            set
            {
                dsUsuarios = value;
            }
        }

        public Frmgestion()
        {
            InitializeComponent();
            bsUsuarios = new BindingSource();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FrmUsuario fp= new FrmUsuario();
            fp.Tblusuarios = dsUsuarios.Tables["Usuario"];
            fp.DsUsuario = dsUsuarios;
            fp.ShowDialog();

        }

        private void Frmgestion_Load(object sender, EventArgs e)
        {
            bsUsuarios.DataSource = dsUsuarios;
            bsUsuarios.DataMember = dsUsuarios.Tables["Usuario"].TableName;
            dgvUsuario.DataSource = bsUsuarios;
            dgvUsuario.AutoGenerateColumns = true;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

            try
            {
                bsUsuarios.Filter = string.Format("Usuario like '*{0}*' or Contraseña like '*{0}*' or Correo like '*{0}*'"
                    , textBox1.Text);

            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
