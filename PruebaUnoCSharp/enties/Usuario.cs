﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaUnoCSharp.enties
{
    class Usuario
    {
        private int id;
        private string nombre_usuario;
        private string contraseña;
        private string confirmar_contraseña;
        private string correo;
        private string confirmar_correo;
        private string telefono;
        private string confirmar_telefono;
        private string cantidad_accesos;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Nombre_usuario
        {
            get
            {
                return nombre_usuario;
            }

            set
            {
                nombre_usuario = value;
            }
        }

        public string Contraseña
        {
            get
            {
                return contraseña;
            }

            set
            {
                contraseña = value;
            }
        }

        public string Confirmar_contraseña
        {
            get
            {
                return confirmar_contraseña;
            }

            set
            {
                confirmar_contraseña = value;
            }
        }

        public string Correo
        {
            get
            {
                return correo;
            }

            set
            {
                correo = value;
            }
        }

        public string Confirmar_correo
        {
            get
            {
                return confirmar_correo;
            }

            set
            {
                confirmar_correo = value;
            }
        }

        public string Telefono
        {
            get
            {
                return telefono;
            }

            set
            {
                telefono = value;
            }
        }

        public string Confirmar_telefono
        {
            get
            {
                return confirmar_telefono;
            }

            set
            {
                confirmar_telefono = value;
            }
        }

        public string Cantidad_accesos
        {
            get
            {
                return cantidad_accesos;
            }

            set
            {
                cantidad_accesos = value;
            }
        }

        public Usuario(int id, string nombre_usuario, string contraseña, string confirmar_contraseña, string correo, string confirmar_correo, string telefono, string confirmar_telefono, string cantidad_accesos)
        {
            this.Id = id;
            this.Nombre_usuario = nombre_usuario;
            this.Contraseña = contraseña;
            this.Confirmar_contraseña = confirmar_contraseña;
            this.Correo = correo;
            this.Confirmar_correo = confirmar_correo;
            this.Telefono = telefono;
            this.Confirmar_telefono = confirmar_telefono;
            this.Cantidad_accesos = cantidad_accesos;
        }
    }
}
